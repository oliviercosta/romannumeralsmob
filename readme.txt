challenge:

write a calculator that takes 2 roman numerals, and returns their sum (as a numeral)

constraints:
- don't use numeric datatypes to do the actual calculations (romans didn't have these either ...)
- don't use primitives in the public signatures of the classes (except as contstructor parameters)
- maximize immutability.  after construction, nothing should be able to change the value of a class
- design solution so that NullPointerExceptions are impossible.
