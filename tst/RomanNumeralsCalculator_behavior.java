
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.stream.Stream;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class RomanNumeralsCalculator_behavior {
    private static Stream<Arguments> examples_verifying_additions() {
        return Stream.of(
                Arguments.of("I","I","II")
        );
    }

    @ParameterizedTest
    @MethodSource("examples_verifying_additions")
    void verify_two_terms(String first, String second, String sum){
        assertEquals(Numeral.createFrom(sum),
                     RomanNumeralCalculator.add(Numeral.createFrom(first),
                                                Numeral.createFrom(second)));
    }
}
