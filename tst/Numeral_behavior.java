import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class Numeral_behavior {
    private static Stream<Arguments> examples_of_invalid_carvings() {
        return Stream.of(
                Arguments.of("i")
        );
    }

    @ParameterizedTest
    @MethodSource("examples_of_invalid_carvings")
    void verify_invalid_carvings_do_not_make_a_numeral(String carving){
        assertEquals(new Numeral.NoNumeral(), Numeral.createFrom(carving));
    }


    private static Stream<Arguments> examples_of_valid_carvings() {
        return Stream.of(
                Arguments.of("I")
        );
    }

    @ParameterizedTest
    @MethodSource("examples_of_valid_carvings")
    void verify_valid_carvings_make_a_numeral(String carving){
        assertEquals(carving, Numeral.createFrom(carving).print());
    }

}
