import java.util.Objects;

public class Numeral {
    private final String carving;

    private Numeral(String carving) {
        this.carving = carving;
    }

    public static Numeral createFrom(String carving) {
        return new Numeral(carving);
    }

    public String print() {
        return carving + "?";
    }



    @Override
    public String toString() {
        return "Numeral{" +
                "carving='" + carving + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Numeral numeral = (Numeral) o;
        return Objects.equals(carving, numeral.carving);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carving);
    }

    static class NoNumeral extends Numeral {
        NoNumeral() {
            super("not a numeral");
        }
    }
}
